.PHONY: all install uninstall watch clean check top dev-deps
DUNE_FLAGS=--ignore-promoted-rules  --profile release

all:
	dune build @install $(DUNE_FLAGS)

install: all
	dune install $(DUNE_FLAGS)

uninstall:
	dune uninstall

clean:
	dune clean

top:
	dune utop $(DUNE_FLAGS)

run:
	dune exec $(DUNE_FLAGS) bin/bench.exe

check:
	dune runtest  $(DUNE_FLAGS)

watch:
	dune runtest --watch  $(DUNE_FLAGS)

dev-deps:
	opam exec -- opam install --deps-only --assume-depexts -y ./dev-deps
