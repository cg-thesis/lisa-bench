open Bechamel
open Toolkit
open Libbench

type test_maker =
  length:int ->
  (module Libbench.Runtime.TEST) ->
  string * (module Libbench.Runtime.LISA_RUNTIME) ->
  Bechamel.Test.t

let pow n m =
  let n, m = (float_of_int n, float_of_int m) in
  n ** m |> floor |> int_of_float

let make_test ~length (module Make : Runtime.TEST)
    (name, (module Impl : Runtime.LISA_RUNTIME)) =
  let module Case = Make (Impl) in
  Test.make ~name (Staged.stage @@ fun () -> Case.bench length (Case.stream ()))

let make_test_indexed ~length (module Make : Runtime.TEST)
    (name, (module Impl : Runtime.LISA_RUNTIME)) =
  let module Case = Make (Impl) in
  let len = length in
  Test.make_indexed ~name
    ~args:(Seq.(ints 1 |> take len) |> List.of_seq)
    (fun n -> Staged.stage @@ fun () -> Case.bench (pow 10 n) (Case.stream ()))

let tests_from (make : test_maker) name (module Case : Runtime.TEST) impls
    ~length =
  Test.make_grouped ~name ~fmt:"%s %s"
    (List.map (make ~length (module Case)) impls)

let benchmark ~limit measures tests =
  let open Benchmark in
  let config = cfg ~limit ~stabilize:true ~quota:(Time.second 60.) () in
  all config measures tests

let fit_curves ~limit measures tests =
  let results = benchmark ~limit measures tests in
  let ols =
    Analyze.ols ~bootstrap:0 ~r_square:true ~predictors:[| Measure.run |]
  in
  let analyze results m = Analyze.all ols m results in

  let results = List.map (analyze results) measures in
  Analyze.merge ols measures results

let measures = ref "all"
let target = ref "8"
let runtimes = ref "all"
let limit = ref "10"
let kind = ref "reg"

let pick_test arg =
  let what =
    match !measures with
    | "time" -> [ Instance.monotonic_clock ]
    | "major" -> [ Instance.major_allocated ]
    | "minor" -> [ Instance.minor_allocated ]
    | "all" -> Instance.[ monotonic_clock; major_allocated; minor_allocated ]
    | "memory" -> Instance.[ major_allocated; minor_allocated ]
    | _ -> failwith "Unknown test kind"
  in
  let stream_length =
    match int_of_string_opt !target with
    | None -> failwith "Incorrect length format"
    | Some n -> n
  in
  let limit =
    match int_of_string_opt !limit with
    | None -> failwith "Incorrect length format"
    | Some n -> n
  in

  let implementations =
    if arg = "natc" || arg = "fibc" then (
      runtimes := "none";
      Implementations.dummy)
    else
      match !runtimes with
      | "none" -> Implementations.dummy
      | "fast" -> Implementations.fast
      | "lazy" -> Implementations.lazyindexed
      | "ref" -> Implementations.reference
      | "noref" -> Implementations.noref
      | _ -> Implementations.all
  in
  let testfn =
    match !kind with "indexed" -> make_test_indexed | _ -> make_test
  in
  let run (module Case : Runtime.TEST) =
    fit_curves ~limit what
      (tests_from testfn arg
         (module Case)
         implementations ~length:stream_length)
    |> IO.print
  in
  match arg with
  | "pi" when !runtimes <> "none" -> run (module Pi.Make)
  | "investors" when !runtimes <> "none" -> run (module Investors.Make)
  | "nat" when !runtimes <> "none" -> run (module Nat.Make)
  | "natc" when !runtimes = "none" -> run (module Stream.Nat)
  | "fib" when !runtimes <> "none" -> run (module Fib.Make)
  | "fibc" when !runtimes = "none" -> run (module Stream.Fib)
  | _ -> raise (Invalid_argument arg)

let spec_list =
  [
    ( "-m",
      Arg.Set_string measures,
      "Set test measures: all; major; minor; memory; time." );
    ("-n", Arg.Set_string target, "Set target observation index.");
    ( "-r",
      Arg.Set_string runtimes,
      "Select runtime selection: all; fast; none; noref; ref." );
    ("-l", Arg.Set_string limit, "Set limit of test iterations.");
    ("-k", Arg.Set_string kind, "Set kind of test: reg; indexed.");
  ]

let usage =
  "usage: bench [-m <measure>] [-k <kind>] [-n <target index>] [-r <runtime \
   selection> ] [-l <limit>] test_name "

let () =
  List.iter
    (fun v -> Bechamel_notty.Unit.add v (Measure.unit v))
    Instance.[ minor_allocated; major_allocated; monotonic_clock ]

let () = Arg.parse spec_list pick_test usage
