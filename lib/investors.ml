open Runtime

module Make : TEST =
functor
  (Impl : LISA_RUNTIME)
  ->
  struct
    module LisaPrelude = MakePrelude (Impl)
    open Impl

    type t = index -> (int * float) list stream

    (* prelude *)
    open LisaPrelude

    (* bench *)
    let stream () =
      let earnings all capital entry =
        let k, v = entry in
        (k, all *. v /. capital)
      in
      let update_entry key f entry =
        let k, v = entry in
        if key = k then (key, f v) else entry
      in
      let update_balance oldv newv = oldv +. newv in
      let assoc_update k v m f =
        if not (mem_assoc k m) then cons (k, v) m
        else mapl (update_entry k (f v)) m
      in
      let manager_id = 0 in
      let from =
        fixs (fun from _ ->
            1 |:: fun () _ ->
            2 |:: fun () _ ->
            3 |:: fun () _ ->
            1 |:: fun () _ ->
            manager_id |:: fun () _ -> manager_id |:: from)
      in
      let amount =
        fixs (fun amount _ ->
            17. |:: fun () _ ->
            23. |:: fun () _ ->
            51. |:: fun () _ ->
            3. |:: fun () _ ->
            0. |:: fun () _ -> 250. |:: amount)
      in
      let active_capital =
        fixs (fun active_capital i ->
            (if now from i = manager_id && now amount i <> 0. then 0.
             else now amount i)
            |:: repeat (fun () i ->
                    if now from i = manager_id && now amount i <> 0. then 0.
                    else pre active_capital i +. now amount i))
      in
      let available_investment =
        fixs (fun available_investment i ->
            (if now from i = manager_id then 0. else now amount i)
            |:: repeat (fun () i ->
                    if now from i = manager_id && now amount i = 0. then 0.
                    else if now from i = manager_id then
                      pre available_investment i
                    else pre available_investment i +. now amount i))
      in
      let ledger =
        fixs (fun ledger i ->
            (if now from i = manager_id then []
             else [ (now from i, now amount i) ])
            |:: repeat (fun () i ->
                    if
                      pre (fun () -> from) i = manager_id
                      && pre (fun () -> amount) i <> 0.
                      && now from i <> manager_id
                    then [ (now from i, now amount i) ]
                    else if now from i <> manager_id then
                      assoc_update (now from i) (now amount i) (pre ledger i)
                        update_balance
                    else pre ledger i))
      in
      let operations =
        fixs (fun _ _ ->
            []
            |:: repeat (fun () i ->
                    if now from i = manager_id && now amount i = 0. then
                      [ (now from i, pre (fun () -> available_investment) i) ]
                    else if now from i = manager_id then
                      mapl
                        (earnings (now amount i)
                           (pre (fun () -> active_capital) i))
                        (now ledger i)
                    else []))
      in
      operations

    let bench n s =
      Sys.opaque_identity
        (ignore (at (fun () -> s) (fun _ -> index_of_int n) o))
  end
