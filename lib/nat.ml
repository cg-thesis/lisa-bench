open Runtime

module Make : TEST =
functor
  (Impl : LISA_RUNTIME)
  ->
  struct
    module LisaPrelude = MakePrelude (Impl)
    open Impl

    (* prelude *)
    open LisaPrelude

    type t = index -> Z.t stream

    (* alloc *)
    let stream () =
      fixs Z.(fun nat _i -> zero |:: repeat (fun () i -> pre nat i + one))

    let bench n s =
      Sys.opaque_identity
        (ignore (at (fun () -> s) (fun _ -> index_of_int n) o))
  end
