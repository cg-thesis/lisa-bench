module Stream = struct
  type 'a t = { head : 'a; tail : 'a node }
  and 'a node = 'a t lazy_t

  let cons head tail = { head; tail }
  let rec nth n s = if n = 0 then s.head else nth (pred n) (Lazy.force s.tail)
end

module Fib : Runtime.TEST =
functor
  (_ : Runtime.LISA_RUNTIME)
  ->
  struct
    open Stream

    type t = Z.t Stream.t

    let stream () =
      let open Z in
      let rec fib n m = cons n (lazy (fib m (n + m))) in
      fib zero one

    let bench n s = Sys.opaque_identity (ignore (nth n s))
  end

module Nat : Runtime.TEST =
functor
  (_ : Runtime.LISA_RUNTIME)
  ->
  struct
    open Stream

    type t = Z.t Stream.t

    let stream () =
      let open Z in
      let rec nat n = cons n (lazy (nat (succ n))) in
      nat zero

    let bench n s = Sys.opaque_identity (ignore (nth n s))
  end
