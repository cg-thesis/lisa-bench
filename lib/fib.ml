open Runtime

module Make : TEST =
functor
  (Impl : LISA_RUNTIME)
  ->
  struct
    module LisaPrelude = MakePrelude (Impl)
    open Impl

    type t = index -> Z.t stream

    (* prelude *)
    open LisaPrelude

    let stream () =
      fixs
        Z.(
          fun fib _i ->
            zero |:: fun () _ ->
            one |:: repeat (fun () i -> pre fib i + pre2 fib i))

    (* bench *)
    let bench n s =
      Sys.opaque_identity
        (ignore (at (fun () -> s) (fun _ -> index_of_int n) o))
  end
